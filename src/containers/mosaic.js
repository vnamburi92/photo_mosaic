import React,{Component} from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createRGBmatrix } from '../actions/index.js';
import { convertRgbColoursToHex } from '../utils/utilFunctions.js';

export class Mosaic extends Component {

  constructor(props){
    super(props)
    this.state = {
      resolvedMatrix : this.props.resolvedMatrix
    }
  }

  render(){
    //When initially rendered, notify for image uploading
    if(this.props.resolvedMatrix.length === 0){
      return <div className="notifyUpload"> Please upload an image</div>
    }
    
    return (
      <div>
        {this.props.resolvedMatrix.map((row,index) => {
          return (
            <div key={index + 'parent'} className='mosaicRow'>
              {row.map((image,index) => <img key={index + 'child'} src={image.src} />)}
            </div>
          );
        })}
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({createRGBmatrix}, dispatch)
};

const mapStateToProps = (state) => {
  return {
    resolvedMatrix : state.mosaic.resolvedMatrix
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Mosaic);