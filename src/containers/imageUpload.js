import React,{Component} from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { splitImage, onImageSampled,emptyMosaic } from '../actions/index.js';
import {imageObjectCreator,cutImageUp,convertDataTilesToRGBTiles,convertRgbColoursToHex} from '../utils/utilFunctions.js';

export class ImageUpload extends Component {
    constructor(props) {
        super(props);
        this.state = {
            file: '',
            imagePreviewUrl: ''
        };
    }

    handleImageChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];

        //Restrict file types
        const fileTypes = {
            "image/png"  : true,
            "image/jpeg" : true
        }

        if(fileTypes[file.type] && file.size < 2000000 ){
            reader.onloadend = () => {
                this.setState({
                    file: file,
                    imagePreviewUrl: reader.result
                });
                //Call to ensure any existing mosaics are clared from redux store

                //Promise based chain leading to the finale action dispatch

                //Empty the current DOM of any existing mosaics
                this.props.emptyMosaic();

                //Convert image to tiels ->
                //Convert Tiles to RGB format ->
                //Convert the RGB format to Hex ->
                //Convert each tile's Hex to a request based promise, resolving to an image
                cutImageUp(imageObjectCreator(reader.result))
                .then((spiltImageData)=>{
                   return convertDataTilesToRGBTiles(spiltImageData)
                })
                .then((RGBMatix)=>{
                    return convertRgbColoursToHex(RGBMatix)
                })
                .then((HexMatrix)=>{
                    this.props.onImageSampled(HexMatrix);
                })
            }
        }
        else{
            reader.onloadend = () => {
                this.setState({
                    file: file,
                    imagePreviewUrl: "https://upload.wikimedia.org/wikipedia/commons/thumb/9/97/Dialog-error-round.svg/2000px-Dialog-error-round.svg.png"
                });
            }
        }

        reader.readAsDataURL(file)
    }

    render() {
        let {imagePreviewUrl} = this.state;
        let $imagePreview = null;
        if (imagePreviewUrl) {
            $imagePreview = (<img src={imagePreviewUrl} />);
        } else {
            $imagePreview = (<div className="previewText">Image should be JPEG OR PNG and less than 1MB</div>);
        }

        return (
            <div className="previewComponent">
                 <button>Push Me...</button>
                 <input type="file" className="button" onChange={(e)=>this.handleImageChange(e)} />
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({splitImage,onImageSampled,emptyMosaic}, dispatch)
};

export default connect(null,mapDispatchToProps)(ImageUpload);
