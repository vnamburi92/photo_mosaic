import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';


export default class App extends Component {

	constructor(props){
		super(props);
		this.state = {
			messageExists : this.props.message.message
		}
	}

	handleClose() {
    // trigger the message being dismissed
	    this.setState({
	    	messageExists : false
	    })
  	}

    render() {

    // In case of no message, return null
    if (!state.messageExists) {
      return null;
    }
    //Based on message type set styling and return message
    return (
      <div className={'message ' + this.props.message.type}>
        <div
          className='close'
          onClick={()=>this.handleClose()}
          dangerouslySetInnerHTML={{__html: '&times;'}}
        />
        {this.props.message.message}
      </div>
    );
  }
}
