import React from 'react';
import { Component } from 'react';
import ImageUpload from '../containers/ImageUpload.js';
import Mosaic from '../containers/mosaic.js';

export default class App extends Component {
  render() {
    return (
        <div>
        	<ImageUpload/>
        	<Mosaic />
        </div>
    );
  }
}
