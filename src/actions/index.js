import {cutImageUp} from '../utils/utilFunctions.js';
import CONSTANTS from '../utils/constants.js';


const {SPLIT_IMAGE,RESOLVED_MATRIX,CLEAR_MOSAIC} = CONSTANTS;

let mosaics = {};

export function splitImage(HexMatrix){
	return {
		type : "SPLIT_IMAGE" ,
		payload : HexMatrix
	}
}

export function createRGBmatrix(mappedPicture){
	return {
		type : RESOLVED_MATRIX ,
		payload : mappedPicture
	}
}

export function loadResolvedTiles(tiles,row){
	return {
		type : RESOLVED_MATRIX ,
		payload : tiles
	}
}

export function emptyMosaic(){
	return {
		type : CLEAR_MOSAIC
	}
}

export function onImageSampled(imageData) {
    var mosaicTilePromises = imageData.map(pixelRowToTilePromises);

    return (dispatch) => {
	 mosaicTilePromises.forEach((tilePromise, rowId) => {
      // TODO: Catch errors
        dispatch(loadResolvedTiles(tilePromise, rowId));
   	 });
    }
     
  }

function pixelRowToTilePromises(row) {
	// Convert the Array of Promises to a single promise which resolves once all are resolved within
	// See: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/all
	return Promise.all(row.map(pixelToPromise))
}

function pixelToPromise(hexColour) {
	if (!mosaics[hexColour]) {
	  mosaics[hexColour] = requestMosaic(hexColour);
	}

	return mosaics[hexColour];
}

// get a Promise resolved with the loaded Image
function requestMosaic(hexColour) {

	var img = new Image,

    result = new Promise(function(resolve) {
      img.onload = function() {
        resolve(img);
      }
      // TODO: Handle timeout
    });

	img.src = '/color/' + hexColour;

	return result;
}