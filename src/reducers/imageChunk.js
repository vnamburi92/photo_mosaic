import CONSTANTS from '../utils/constants.js';
const {SPLIT_IMAGE} = CONSTANTS;

const INITIAL_STATE = {imageArray: [] };

//Object.assign used to return a fresh object instance, preventing and current state-object mutation
//Switch statement allows for redux filtering and reducer targetting
export default function (state=INITIAL_STATE,action){
    switch (action.type){
        case SPLIT_IMAGE: {
        	return Object.assign({},state,{imageArray : action.payload });
        }
    }

    return state;
}