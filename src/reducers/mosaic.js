import CONSTANTS from '../utils/constants.js';
const {RESOLVED_MATRIX,CLEAR_MOSAIC} = CONSTANTS;

const INITIAL_STATE = {resolvedMatrix: []};

//Object.assign used to return a fresh object instance, preventing and current state-object mutation
export default function (state=INITIAL_STATE,action){
    switch (action.type){
        case RESOLVED_MATRIX: {
        	return Object.assign({},state,{resolvedMatrix : state.resolvedMatrix.concat([action.payload]) });
        }
        case CLEAR_MOSAIC: {
        	return Object.assign({},state,{resolvedMatrix : [] });
        }
    }

    return state;
}