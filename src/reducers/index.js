import { combineReducers } from 'redux';
import ImageChunk from './imageChunk.js';
import mosaic from './mosaic.js';


const rootReducer = combineReducers({
  ImageChunk,
  mosaic
});

export default rootReducer;
