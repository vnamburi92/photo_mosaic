import {TILE_WIDTH,TILE_HEIGHT} from './constants.js';

//Uses canvas to draw individual tiles respective to the grid height,witdh
export function cutImageUp(image,grid) {
	return new Promise((resolve,reject)=>{
	const xTiles = Math.ceil(image.width / TILE_WIDTH);
	const yTiles = Math.ceil(image.height / TILE_HEIGHT);
	let imagePieces = [];
    for(let x = 0; x < yTiles; ++x) {
    	var imagePieceRow = [];
        for(let y = 0; y < xTiles; ++y) {
            imagePieceRow.push(calculateImageData(image,x,y));
        }
    imagePieces.push(imagePieceRow);
    }
    resolve(imagePieces)
	})
}

function createCanvasContext(dimensions){
	var canvas = document.createElement('canvas');
	if(!!dimensions){
		canvas.width = dimensions.width;
    	canvas.height = dimensions.height;
	}
	return canvas.getContext('2d');
	
}

//based on the parent image object and the position in the grid, a tile is created 
//and it's pixelData is returned
function calculateImageData(image,x,y){

	let context = createCanvasContext({width : TILE_WIDTH, height : TILE_HEIGHT})

    //https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Using_images
    //Taking the image base64 source and cutting it based on constraints provided
    //Each slice is a canvas in itself, as extended methods allow to convert the image to base 64
    context.drawImage(image, y * TILE_WIDTH, x * TILE_HEIGHT, TILE_WIDTH, TILE_HEIGHT,0,0,TILE_WIDTH, TILE_HEIGHT);
    return context.getImageData(0, 0, TILE_WIDTH, TILE_HEIGHT).data

}


//function convers base 64 encoding to image object to allow for canvas recognition
export function imageObjectCreator(base64Url){
	const image = new Image();
	image.src = base64Url;
	return image;
}

//promise based function that converts all the present data based tiles to RGB by calling getAverageRGB()
export function convertDataTilesToRGBTiles(sampledImage){
	let RGBMatix = sampledImage.map((row)=> {
      return row.map((tile)=>{
      	return getAverageRGB(tile)
      }) 
 	 })

	return new Promise((resolve,reject)=>{
		resolve(RGBMatix);
	})
}

// Loops through hexmatrix applying the rgbToHex conversion function in a promise environment
export function convertRgbColoursToHex(rgbs) {
  let HexMatrix = rgbs.map((row) => {
    return row.map(pixel => {
    	return rgbToHex.apply(null,pixel);
    });
  });

  return new Promise((resolve,reject)=>{
		resolve(HexMatrix);
	})
}

// The (1 << 24) and .substr(1) ensure it is left-padded with 0's if needed
function rgbToHex(red, green, blue) {
  return ((1 << 24) + (red << 16) + (green << 8) + blue).toString(16).substr(1);
}

//Loops through pixelData array, aggregating all respective r,g,b values and returning an array pattern of the floored averages of each
export function getAverageRGB(tilePixelArray) {
    
    var blockSize = 1, // visit every pixel
        defaultRGB = {r:0,g:0,b:0}, // for non-supporting envs
        i = -4,
        length,
        rgb = {r:0,g:0,b:0},
        count = 0;
    
    while ( (i += blockSize * 4) < tilePixelArray.length ) {
        ++count;
        rgb.r += tilePixelArray[i];
        rgb.g += tilePixelArray[i+1];
        rgb.b += tilePixelArray[i+2];
    }

    // ~~ used to floor values
    rgb.r = Math.round(rgb.r/count);
    rgb.g = Math.round(rgb.g/count);
    rgb.b = Math.round(rgb.b/count);
    return [rgb.r,rgb.g,rgb.b]
    
}


