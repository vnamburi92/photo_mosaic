// Constants shared between client and server.

var TILE_WIDTH = 16;
var TILE_HEIGHT = 16;

var exports = exports || module.exports || null;
if (exports) {
    exports.TILE_WIDTH = TILE_WIDTH;
    exports.TILE_HEIGHT = TILE_HEIGHT;
    exports.SPLIT_IMAGE = 'SPLIT_IMAGE';
    exports.RESOLVED_MATRIX = 'RESOLVED_MATRIX';
    exports.CLEAR_MOSAIC = 'CLEAR_MOSAIC';
}

